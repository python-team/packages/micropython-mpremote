.TH mpremote 1
.
.SH NAME
mpremote \- MicroPython remote control
.
.
.SH SYNOPSIS
.SY mpremote
command
.OP options
.OP "+ command" ...
.YS
.
.SY mpremote
.OP \-\-help
.YS
.
.SY mpremote
.OP \-\-version
.YS
.
.
.SH DESCRIPTION
The
.B mpremote
command line tool provides an integrated set of utilities to remotely interact
with and automate a MicroPython device over a serial connection.
.
.PP
The simplest way to use this tool is just by invoking it without any arguments:
.
.RS
.PP
.EX
mpremote
.EE
.RE
.
.PP
This command automatically detects and connects to the first available serial
device and provides an interactive REPL.
Serial ports are opened in exclusive mode, so running a second (or third, etc)
instance of
.B mpremote
will connect to subsequent serial devices, if any are available.
.
.
.SH OPTIONS
.
.TP
.B \-\-help
Show a help message and exit.
.
.TP
.B \-\-version
Show the program version and exit.
.
.
.SH COMMANDS
For REPL access, running
.B mpremote
without any arguments is usually all that is
needed.
.B mpremote
also supports a set of commands given at the command line which will perform
various actions on remote MicroPython devices.
.
.PP
For commands that support multiple arguments (e.g. a list of files), the
argument list can be terminated with
.BR + .
.
The full list of supported commands are:
.
.TP
.BR connect " [\fIdevice\fR]"
Connect to a specified device via a device-name shortcut.
.I device
may be one of:
.
.RS
.TP
auto
connect to the first available device
.
.TP
list
list the available devices
.
.TP
.RI id: serial
connect to the device with USB serial number
.I serial
(the second entry in the output from the
.B connect list
command)
.
.TP
.RI port: path
connect to the device with the given path
.RE
.
.IP
If the
.I device
is not specified, auto is assumed.
.
.TP
.B disconnect
Disconnects the current device. After a disconnect, auto
.B soft-reset
is enabled (see
.B AUTO-CONNECTION AND SOFT-RESET
below).
.
.TP
.B resume
Resume a previous
.B mpremote
session. This disables auto
.B soft-reset
(see
.B AUTO-CONNECTION AND SOFT-RESET
below).
.
.TP
.B soft-reset
Perform a soft-reset of the device.
This will clear out the Python heap and restart the interpreter.
It also disables auto
.B soft-reset
(see
.B AUTO-CONNECTION AND SOFT-RESET
below).
.
.TP
.BR repl " [\fIoptions\fR]"
Enter the REPL on the connected device.
The available
.I options
are:
.
.RS
.TP
.BI \-\-capture " file"
to capture output of the REPL session to the given file
.
.TP
.BI \-\-inject\-code " string"
to specify characters to inject at the REPL when
.B Ctrl-J
is pressed
.
.TP
.BI \-\-inject\-file " file"
to specify a file to inject at the REPL when
.B Ctrl-K
is pressed
.RE
.
.TP
.BI eval " string"
Evaluate ahd print the result of a Python expression.
.
.TP
.BI exec " string"
Execute the given Python code.
.
.TP
.BI run " file"
Run a script from the local filesystem.
.
.TP
.BI fs " subcommand"
Execute filesystem commands on the device.
.I subcommand
may be:
.
.RS
.TP
.BI cat " files..."
to show the contents of a file or files on the device
.
.TP
.B ls
.TQ
.BI ls " dirs..."
to list the current, or specified directories
.
.TP
.BR cp " [\fI-r\fR]\fI src... dest"
to copy files; use ":" as a prefix to specify a file on the device
.
.TP
.BI rm " src..."
to remove files on the device
.
.TP
.BI mkdir " dirs..."
to create directories on the device
.
.TP
.BI rmdir " dirs..."
to remove directories on the device
.
.TP
.BI touch " files..."
to create empty files (if they don't already exist)
.RE
.
.TP
.BI edit " files..."
Edit a file (or files) on the device. The
.B edit
command will copy each file from the device to a local temporary directory and
then launch your editor (defined by the environment variable
.IR $EDITOR ).
If the editor exits successfully, the updated file will be copied back to the
device.
.
.TP
.BR "mip install" " [\fIoptions\fR]\fI packages..."
Install packages from micropython-lib (or GitHub) using the
.B mip
tool.
The available
.I options
are:
.
.RS
.TP
.BI \-\-target " path"
Install under
.I path
instead of the default location (the first entry in
.B sys.path
ending in
.BR /lib ).
Please note that
.I
must exist in
.B sys.path
for you to be able to subsequently import the package.
.
.TP
.B \-\-no\-mpy
Install the source version 
.I .py
files rather than
.IR .mpy).
.
.TP
.BI \-\-index " URL"
Use the specified
.I URL
instead of the default package index at
.BR https://\:micropython.org/\:pi .
.RE
.
.TP
.BR mount " [\fIoptions\fR]\fI local-dir"
Mount a local directory on the remote device (under the path
.BR /remote ).
During usage,
.B Ctrl-D
will soft-reboot and normally reconnect the mount automatically.
If the unit has a
.I main.py
running at startup however, the remount cannot occur.
In this case a raw mode soft reboot can be used:
.B Ctrl-A Ctrl-D
to reboot, then
.B Ctrl-B
to get back to normal repl at which point the mount will be ready.
.
.IP
The available
.I options
are:
.
.RS
.TP
.BR \-l ", " \-\-unsafe\-links
By default an error will be raised if the device accesses a file or directory
which is outside (up one or more directory levels) the local directory that is
mounted.
This option disables this check for symbolic links, allowing the device to
follow symbolic links outside of the local directory.
.RE
.
.TP
.B umount
Unmount the local directory from the remote device.
.
.
.SH AUTO-CONNECTION AND SOFT-RESET
Connection and disconnection will be done automatically at the start and end of
the execution of the tool, if such commands are not explicitly given.
Automatic connection will search for the first available serial device.
If no action is specified then the REPL will be entered.
.
.PP
Once connected to a device,
.B mpremote
will automatically soft-reset the device if needed.
This clears the Python heap and restarts the interpreter, making sure that
subsequent Python code executes in a fresh environment.
Auto soft-reset is performed the first time one of the following commands are
executed:
.BR mount ", " eval ", " exec ", " run ", " fs .
After doing a soft-reset for the first time, it will not be done again
automatically, until a disconnect command is issued.
.
.PP
Auto soft-reset behaviour can be controlled by the
.B resume
command. And the
.B soft-reset
command can be used to perform an explicit soft reset.
.
.
.SH SHORTCUTS
Shortcuts can be defined using the macro system. Built-in shortcuts are:
.
.RS
.TP
.B devs
list available devices (shortcut for
.BR "connect list" )
.
.TP
.BR a0 ", " a1 ", " a2 ", " a3
connect to
.RI /dev/ttyACM n
.
.TP
.BR u0 ", " u1 ", " u2 ", " u3
connect to
.RI /dev/ttyUSB n
.
.TP
.BR c0 ", " c1 ", " c2 ", " c3
connect to
.RI COM n
.
.TP
.BR cat ", " ls ", " cp ", " rm ", " mkdir ", " rmdir ", " touch ", " df
filesystem commands (mostly aliases for the
.B fs
equivalent)
.
.TP
.B reset
reset the device
.
.TP
.B bootloader
make the device enter its bootloader
.RE
.
.PP
Any user configuration, including user-defined shortcuts, can be placed in the
file
.BR ~/.config/mpremote/config.py .
For example:
.
.RS
.PP
.EX
commands = {
    "c33": "connect id:334D335C3138",
    "bl": "bootloader",
    "double x=4": "eval x*2",  # x is an argument, with default 4
    "wl_scan": ["exec", """
import network
wl = network.WLAN()
wl.active(1)
for ap in wl.scan():
    print(ap)
""",],
    "test": ["mount", ".", "exec", "import test"],
}
.EE
.RE
.
.
.SH EXAMPLES
.TP
.B mpremote
Launch a repl on the automatically detected device.
.
.TP
.B mpremote a1
Launch a repl on the device connected to port
.B /dev/ttyACM1
(using the built-in
.B a1
shortcut).
.
.TP
.B mpremote connect /dev/ttyUSB0 repl
Launch a repl on the device connect to port
.BR /dev/ttyUSB0 .
.
.TP
.B mpremote ls
List files on the automatically detected device.
.
.TP
.B mpremote a1 ls
List files on the device connected to port
.B /dev/ttyACM1
(using the built-in
.B a1
shortcut).
.
.TP
.B mpremote exec "import micropython; micropython.mem_info()"
Execute some Python code on the remote board to obtain various memory
statistics.
.
.TP
.B mpremote eval 1/2 eval 3/4
Execute (and show the output of) two simple Python statements on the remote
board. Note that no
.B +
was required because
.B eval
accepts a single string argument.
.
.TP
.B mpremote mount .
Mount the local directory under
.B /remote
on the automatically detected device.
.
.TP
.B mpremote mount . exec \(dqimport localscript\(dq
Mount the local directory under
.B /remote
on the MicroPython board, then import the "localscript" module
.
.TP
.B mpremote cat boot.py
Show the contents of
.B boot.py
on the MicroPython board.
.
.TP
.B mpremote cp :main.py .
Copy the
.B main.py
file from the (remote) MicroPython board to the current (local) directory.
.
.TP
.B mpremote cp main.py :
Copy the
.B main.py
from the current (local) directory to the (remote) MicroPython board.
.
.TP
.B mpremote cp :a.py :b.py
On the MicroPython board, copy
.B a.py
to
.BR b.py .
.
.TP
.B mpremote cp -r dir/ :
Recursively copy the local directory
.B dir/
to the root directory of the (remote) MicroPython board.
.
.TP
.B mpremote cp a.py b.py : + repl
Copy the local files
.B a.py
and
.B b.py
to the root directory of the (remote) MicroPython board, then launch a repl.
Note that the
.B +
terminator is required here as the
.B cp
command takes an arbitrary number of parameters.
.
.TP
.B mpremote mip install aioble
Install the asyncio-based wrapper for the Bluetooth API.
.
.TP
.B mpremote mip install github:org/repo@branch
Install the
.B branch
of the MicroPython package located under
.B org/repo
on GitHub.
.
.TP
.B mpremote mip install --target /flash/third-party functools
Install the
.B functools
package under the
.B /flash/third-party
directory on the MicroPython board.
.
.
.SH SEE ALSO
.BR micropython (1)
